package com.core.dto.khabarOnline

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import com.core.dto.javaRss.RssChannelDto
import kotlinx.android.parcel.Parcelize
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root


@Root(name = "rss", strict = false)
@TypeConverters(OperatorTypeConverters::class)
@Parcelize
class KhabarFeedDto(): Parcelable {

    @field:Element(name = "channel")
    var channel: KhabarChannelDto? = null

    override fun toString(): String {
        return "RssFeed [channel=$channel]"
    }
}
