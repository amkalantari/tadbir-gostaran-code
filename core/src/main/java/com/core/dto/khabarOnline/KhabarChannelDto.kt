package com.core.dto.khabarOnline

import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import com.core.dto.javaRss.RssImageDto
import com.core.dto.javaRss.RssItemDto
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root


@Root(name = "channel", strict = false)
@TypeConverters(OperatorTypeConverters::class)
class KhabarChannelDto {
    @field:Element(name = "title")
    var title: String? = null

    @field:Element(name = "description")
    var description: String? = null

    @field:ElementList(inline = true, required = false)
    var item: List<KhabarItemDto>? = null

    override fun toString(): String {
        return "Channel [item=$item]"
    }
}