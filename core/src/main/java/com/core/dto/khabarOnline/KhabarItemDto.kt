package com.core.dto.khabarOnline

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import kotlinx.android.parcel.Parcelize
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
@TypeConverters(OperatorTypeConverters::class)
@Parcelize
@Entity
class KhabarItemDto():Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:Element(name = "title")
    var title: String? = null

    @field:Element(name = "link")
    var link: String? = null

    @field:Element(name = "pubDate")
    var pubDate: String? = null

    @field:Element(name = "description")
    var description: String? = null

    @field:Element(name = "enclosure",required = false)
    var enclosure: KhabarOnlineImageDto? = null

    var isLike : Boolean = false

    class RssItemDiff : DiffUtil.ItemCallback<KhabarItemDto>() {
        override fun areItemsTheSame(oldItem: KhabarItemDto, newItem: KhabarItemDto): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: KhabarItemDto, newItem: KhabarItemDto): Boolean =
            oldItem.title == newItem.title
    }

    override fun toString(): String {
        return ("RssItem [title=" + title + ", link=" + link + ", pubDate=" + pubDate
                + ", description=" + description + "]")
    }
}