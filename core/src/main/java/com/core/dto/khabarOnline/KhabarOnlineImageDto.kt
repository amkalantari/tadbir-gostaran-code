package com.core.dto.khabarOnline

import android.os.Parcelable
import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import kotlinx.android.parcel.Parcelize
import org.simpleframework.xml.Attribute

@TypeConverters(OperatorTypeConverters::class)
@Parcelize
class KhabarOnlineImageDto(): Parcelable {

    @get:Attribute(name = "type")
    @set:Attribute(name = "type")
    var type: String? = null

    @get:Attribute(name = "url")
    @set:Attribute(name = "url")
    var url: String? = null

    override fun toString(): String {
        return "KhabarOnlineImageDto []"
    }
}