package com.core.dto.javaRss

import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root


@Root(name = "channel", strict = false)
@TypeConverters(OperatorTypeConverters::class)
class RssChannelDto {
    @field:Element(name = "title")
    var title: String? = null

    @field:Element(name = "image")
    var image: RssImageDto? = null

    @field:ElementList(inline = true, required = false)
    var item: List<RssItemDto>? = null

    override fun toString(): String {
        return "Channel [image=$image, item=$item]"
    }
}