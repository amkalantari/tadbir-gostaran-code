package com.core.dto.javaRss

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
@TypeConverters(OperatorTypeConverters::class)
@Entity
class RssItemDto {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:Element(name = "title")
    var title: String? = null

    @field:Element(name = "link")
    var link: String? = null

    @field:Element(name = "pubDate")
    var pubDate: String? = null

    @field:Element(name = "description")
    var description: String? = null

    var isLike : Boolean = false

    class RssItemDiff : DiffUtil.ItemCallback<RssItemDto>() {
        override fun areItemsTheSame(oldItem: RssItemDto, newItem: RssItemDto): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: RssItemDto, newItem: RssItemDto): Boolean =
            oldItem.title == newItem.title
    }

    override fun toString(): String {
        return ("RssItem [title=" + title + ", link=" + link + ", pubDate=" + pubDate
                + ", description=" + description + "]")
    }
}