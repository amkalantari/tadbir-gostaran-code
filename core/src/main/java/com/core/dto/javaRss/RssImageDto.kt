package com.core.dto.javaRss

import androidx.room.TypeConverters
import com.core.db.OperatorTypeConverters
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "image", strict = false)
@TypeConverters(OperatorTypeConverters::class)
class RssImageDto {

    @field:Element(name = "url")
    var url: String? = null

    @field:Element(name = "width")
    var width: String? = null

    @field:Element(name = "height")
    var height: String? = null


    override fun toString(): String {
        return "RssImage [url=$url, width=$width, height=$height]"
    }
}