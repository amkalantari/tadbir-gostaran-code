package com.core.base


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.core.dto.Status


abstract class ParentSharedFragment<T : BaseViewModel, E : ViewDataBinding> : BaseFragment() {

    lateinit var viewModel: T

    lateinit var dataBinding: E

    abstract fun getViewModelClass(): Class<T>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.run {
            viewModel = ViewModelProvider(this).get(getViewModelClass())
            viewModel.getNetworkStatus().observe(this, Observer {
                when (it.status) {
                    Status.RUNNING -> showProgress(it.tag ?: "")
                    Status.SUCCESS -> hideProgress(it.tag ?: "")
                    else -> showError(it.tag ?: "", it.msg ?: getString(it.event))
                }
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            getResourceLayoutId(),
            container,
            false
        )
        viewModel.onCreated()
        return dataBinding.root
    }

    open fun onBackPressed() {
        requireActivity().onBackPressed()
    }
}