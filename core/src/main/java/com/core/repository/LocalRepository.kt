package com.core.repository

import androidx.lifecycle.LiveData
import com.core.base.BaseRepository
import com.core.dao.KhabarDao
import com.core.dao.RssDao
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto

abstract class LocalRepository : BaseRepository() {
    abstract fun getRss() :LiveData<List<RssItemDto>>
    abstract fun insertRss(rss: List<RssItemDto>, onCompleted: (() -> Unit)?, onError: ((Throwable) -> Unit)?)
    abstract fun updateRss(rss: RssItemDto)
    abstract fun getFeed():LiveData<List<KhabarItemDto>>
    abstract fun insertFeed(khabar: List<KhabarItemDto>, onCompleted: (() -> Unit)?, onError: ((Throwable) -> Unit)?)
    abstract fun updateFeed(khabar: KhabarItemDto)
    abstract fun getFavoriteRss():LiveData<List<RssItemDto>>
}

class LocalRepositoryImpl(
    private val feedDao: KhabarDao,
    private val rssDao: RssDao
) : LocalRepository() {

    override fun getRss(): LiveData<List<RssItemDto>> = rssDao.findRss()

    override fun insertRss(rss: List<RssItemDto>, onCompleted: (() -> Unit)?, onError: ((Throwable) -> Unit)?) {
        addExecutorThreads(rssDao.insertRss(rss), onCompleted, onError)
    }

    override fun updateRss(
        rss: RssItemDto
    ) {
        addExecutorThreads(rssDao.updateRss(rss))
    }

    override fun getFeed(): LiveData<List<KhabarItemDto>> = feedDao.findKhabar()

    override fun insertFeed(khabar: List<KhabarItemDto>, onCompleted: (() -> Unit)?, onError: ((Throwable) -> Unit)?) {
        addExecutorThreads(feedDao.insertKhabar(khabar), onCompleted, onError)
    }

    override fun updateFeed(
        khabar: KhabarItemDto
    ) {
        addExecutorThreads(feedDao.updateKhabar(khabar))
    }

    override fun getFavoriteRss() = rssDao.getFavorite()

}