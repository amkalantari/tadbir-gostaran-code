package com.core.repository

import androidx.lifecycle.MutableLiveData
import com.core.api.RssApi
import com.core.base.BaseRepository
import com.core.dao.RssDao
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssItemDto

abstract class RssRepository : BaseRepository() {
    abstract fun getRss(page: String): Response<List<RssItemDto>>
}

class RssRepositoryImpl(
    private val rssApi: RssApi,
    private val localRepository: LocalRepository
) : RssRepository() {

    override fun getRss(page: String): Response<List<RssItemDto>> {
        val TAG = "${this::class.java.simpleName}_login"
        val data = localRepository.getRss()
        addExecutorThreads(rssApi.feed(), onSuccess = {
            localRepository.insertRss(it.channel!!.item!!,{}){
                handleError(TAG, it)
            }
        }, onError = {
            handleError(TAG, it)
        })
        return Response(data, networkStatus)
    }

}