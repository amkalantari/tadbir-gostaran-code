package com.core.repository

import com.core.api.KhabarOnlineRssApi
import com.core.base.BaseRepository
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto

abstract class KhabarOnlineRssRepository : BaseRepository() {
    abstract fun getRss(page: String): Response<List<KhabarItemDto>>
}

class KhabarOnlineRssRepositoryImpl(
    private val khabarOnlineRssApi: KhabarOnlineRssApi,
    private val localRepository: LocalRepository
) : KhabarOnlineRssRepository() {

    override fun getRss(page: String): Response<List<KhabarItemDto>> {
        val TAG = "${this::class.java.simpleName}_getRss"
        val data = localRepository.getFeed()
        addExecutorThreads(khabarOnlineRssApi.rss(), onSuccess = {
            localRepository.insertFeed(it.channel!!.item!!, {
                //success insert to db
            }) {
                handleError(TAG, it)
            }
        }, onError = {
            handleError(TAG, it)
        })
        return Response(data, networkStatus)
    }

}