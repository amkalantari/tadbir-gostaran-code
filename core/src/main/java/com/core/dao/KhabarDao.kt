package com.core.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto
import io.reactivex.Completable

@Dao
interface KhabarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKhabar(feedDTO: List<KhabarItemDto>) : Completable

    @Query("SELECT * FROM khabaritemdto")
    fun findKhabar(): LiveData<List<KhabarItemDto>>

    @Update()
    fun updateKhabar(khabarItemDto: KhabarItemDto): Completable

}