package com.core.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarItemDto
import io.reactivex.Completable

@Dao
interface RssDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRss(rss: List<RssItemDto>) : Completable

    @Query("SELECT * FROM rssitemdto")
    fun findRss(): LiveData<List<RssItemDto>>

    @Query("SELECT * FROM rssitemdto WHERE isLike = 1")
    fun getFavorite(): LiveData<List<RssItemDto>>

    @Update()
    fun updateRss(rssItemDto: RssItemDto): Completable
}