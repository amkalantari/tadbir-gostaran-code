package com.core.utils

import android.annotation.SuppressLint
import android.app.Application
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.os.Looper
//import com.google.android.gms.common.ConnectionResult
//import com.google.android.gms.common.api.GoogleApiClient
//import com.google.android.gms.location.*


class LocationHelper(private val application: Application)
//    : GoogleApiClient.ConnectionCallbacks,
//    GoogleApiClient.OnConnectionFailedListener {
//
//    private var locationCallback: LocationCallback? = null
//
//    var googleApiClient: GoogleApiClient = GoogleApiClient.Builder(application)
//        .addApi(LocationServices.API)
//        .addConnectionCallbacks(this)
//        .addOnConnectionFailedListener(this)
//        .build()
//
//    lateinit var mLocationRequest: LocationRequest
//
//    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
//
//    private val INTERVAL = (1000 * 10).toLong()
//
//    private val FASTEST_INTERVAL = (1000 * 5).toLong()
//
//    init {
//        googleApiClient.connect()
//    }
//
//    private fun createLocationRequest() {
//        mLocationRequest = LocationRequest.create()
//        mLocationRequest
//            .setInterval(INTERVAL)
//            .setFastestInterval(FASTEST_INTERVAL)
//            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//
//        locationCallback = object : LocationCallback() {
//            override fun onLocationResult(locationResult: LocationResult?) {
//                super.onLocationResult(locationResult)
//                if (locationResult == null) {
//                    return
//                }
//                for (newLocation in locationResult.getLocations()) {
//                    if (newLocation != null) {
//                        //locCallBack.updateUi(newLocation)
//                    }
//                }
//            }
//
//            override fun onLocationAvailability(p0: LocationAvailability?) {
//                super.onLocationAvailability(p0)
//            }
//        }
//    }
//
//    @SuppressLint("MissingPermission")
//    private fun setLocationProvider() {
//        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(application)
//        fusedLocationProviderClient.requestLocationUpdates(
//            mLocationRequest,
//            locationCallback,
//            Looper.myLooper()
//        )
//    }
//
//    override fun onConnected(bundle: Bundle?) {
//        createLocationRequest()
//        setLocationProvider()
//    }
//
//    override fun onConnectionSuspended(i: Int) {
//        //locCallBack.updateUi(null)
//    }
//
//    override fun onConnectionFailed(connectionResult: ConnectionResult) {
//        println(
//            connectionResult.errorCode.toString() + " " + connectionResult.errorMessage
//        )
//        //   locCallBack.updateUi(null);
//        if (connectionResult.hasResolution()) {
//            try {
//                //connectionResult.startResolutionForResult(activity, 6)
//            } catch (e: SendIntentException) {
////                locCallBack.updateUi(null)
//            }
//        } else {
//            //Connection failure cannot be resolved
////            Toast.makeText(
////                activity,
////                "Unable to connect to Google Service, sorry >_<" + connectionResult.getErrorMessage(),
////                Toast.LENGTH_LONG
////            )
////                .show()
////            Log.d(
////                "CONNECTION FAIL",
////                connectionResult.getErrorCode()
////                    .toString() + " Unable to connect to Google Service, sorry >_<"
////            )
////            locCallBack.updateUi(null)
//        }
//    }
//
//}
