package com.core.utils

interface SettingManager {

   fun setDirectory(path : String)
   fun getDirectory() : String

    fun setDefaultName(name : String)
    fun getDefaultName() : String

    fun setDefaultExtension(extension : String)
    fun getDefaultExtension() : String

    fun setPrivacyPolicy(accepted : Boolean)
    fun getPrivacyPolicy() : Boolean
}
class SettingManagerImpl(private val defaultPath : String, private val preference: Preference) : SettingManager {

    companion object {
        private const val privacy_policy= "privacy_policy"
        private const val default_path  = "default_path"
        private const val default_name  = "default_name"
        private const val default_extension  = "default_extension"
    }

    override fun setDirectory(path: String) {
        preference.put(default_path, path)
    }

    override fun getDirectory(): String {
        val path  = preference.getString(default_path)
        if (path.isEmpty()){
            return defaultPath
        }
        return path
    }

    override fun setDefaultName(name: String) {
        preference.put(default_name, name)
    }

    override fun getDefaultName(): String {
        val name = preference.getString(default_name)
        if (name.isEmpty()){
            return "Audio_voice"
        }
        return name
    }

    override fun setDefaultExtension(extension: String) {
        preference.put(default_extension, extension)
    }

    override fun getDefaultExtension(): String {
        val ext = preference.getString(default_extension)
        if (ext.isEmpty()){
            return "3gp"
        }
        return ext
    }

    override fun setPrivacyPolicy(accepted: Boolean) {
        preference.put(privacy_policy, accepted)
    }

    override fun getPrivacyPolicy(): Boolean = preference.getBoolean(privacy_policy)
}

