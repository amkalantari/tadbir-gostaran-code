package com.core.api

import com.core.dto.javaRss.RssFeedDto
import io.reactivex.Observable
import retrofit2.http.GET

interface RssApi {

    @GET("feed")
    fun feed() : Observable<RssFeedDto>

}