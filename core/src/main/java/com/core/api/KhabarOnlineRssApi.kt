package com.core.api

import com.core.dto.javaRss.RssFeedDto
import com.core.dto.khabarOnline.KhabarFeedDto
import io.reactivex.Observable
import retrofit2.http.GET

interface KhabarOnlineRssApi {

    @GET("rss")
    fun rss() : Observable<KhabarFeedDto>

}