package com.core.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.core.dao.KhabarDao
import com.core.dao.RssDao
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto

/**
 * Main database description.
 */
@Database(
    entities =
    [
        RssItemDto::class,
        KhabarItemDto::class
    ],
    version = 1, exportSchema = false
)
abstract class TestDb : RoomDatabase() {

    abstract fun rssDao(): RssDao
    abstract fun khabarDao(): KhabarDao

}
