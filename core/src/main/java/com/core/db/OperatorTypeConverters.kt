package com.core.db

import android.util.Log
import androidx.room.TypeConverter
import com.core.dto.javaRss.RssChannelDto
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssImageDto
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarChannelDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto
import com.core.dto.khabarOnline.KhabarOnlineImageDto
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


object OperatorTypeConverters {

    @TypeConverter
    @JvmStatic
    fun stringToIntList(data: String?): List<Int>? {
        return data?.let {
            it.split(",").map {
                try {
                    it.toInt()
                } catch (ex: NumberFormatException) {
                    Log.e("OperatorTypeConverters","Cannot convert ${ex.message} to number")
                    null
                }
            }
        }?.filterNotNull()
    }


    @TypeConverter
    @JvmStatic
    fun stringToRssFeedDto(value: String): RssFeedDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun RssFeedDtoToString(items: RssFeedDto?): String = toJson(items)


    @TypeConverter
    @JvmStatic
    fun stringToRssChannelDto(value: String): RssChannelDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun RssChannelDtoToString(items: RssChannelDto?): String = toJson(items)

    @TypeConverter
    @JvmStatic
    fun stringToRssImageDto(value: String): RssImageDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun RssImageDtoToString(items: RssImageDto?): String = toJson(items)

    @TypeConverter
    @JvmStatic
    fun stringToRssItemDto(value: String): RssItemDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun RssItemDtoToString(items: RssItemDto?): String = toJson(items)


    @TypeConverter
    @JvmStatic
    fun stringToKhabarFeedDto(value: String): KhabarFeedDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun KhabarFeedDtoToString(items: KhabarFeedDto?): String = toJson(items)

    @TypeConverter
    @JvmStatic
    fun stringToKhabarChannelDto(value: String): KhabarChannelDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun KhabarChannelDtoToString(items: KhabarChannelDto?): String = toJson(items)

    @TypeConverter
    @JvmStatic
    fun stringToKhabarItemDto(value: String): KhabarItemDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun KhabarItemDtoToString(items: KhabarItemDto?): String = toJson(items)

    @TypeConverter
    @JvmStatic
    fun stringToKhabarOnlineImageDto(value: String): KhabarOnlineImageDto = fromJson(value)

    @TypeConverter
    @JvmStatic
    fun KhabarOnlineImageDtoToString(items: KhabarOnlineImageDto?): String = toJson(items)


    inline fun <reified T> toJson(value: T): String {
        return if (value == null) "" else Gson().toJson(value)
    }

    inline fun <reified T> fromJson(value: String): T {
        return Gson().fromJson(value, object : TypeToken<T>() {}.type)
    }

}

