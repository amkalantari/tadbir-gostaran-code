package com.amiir.testapp.ui.main

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.amiir.testapp.R
import com.amiir.testapp.ui.feedTabs.FirstFeedTabFragment
import com.amiir.testapp.ui.feedTabs.SecondFeedTabFragment


class TabsPagerAdapter(context: Context, fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {

    @StringRes
    private val TAB_TITLES =
        intArrayOf(R.string.first_tab, R.string.second_tab)

    private var mContext = context

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> FirstFeedTabFragment()
            1 -> SecondFeedTabFragment()
            else -> FirstFeedTabFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mContext.resources.getString(TAB_TITLES[position])
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
        return POSITION_NONE
    }

}