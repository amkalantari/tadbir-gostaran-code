package com.amiir.testapp.ui.main.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.core.dto.NetworkState
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto
import com.core.repository.KhabarOnlineRssRepository
import com.core.repository.LocalRepository
import com.core.repository.RssRepository

class MainViewModelImpl (
    application: Application,
    var rssRepository: RssRepository,
    var khabarOnlineRssRepository: KhabarOnlineRssRepository,
    var localRepository: LocalRepository
) : MainViewModel(application) {

    private val feed: MutableLiveData<String> = MutableLiveData()
    private val rss: MutableLiveData<String> = MutableLiveData()

    override fun getNetworkStatus(): LiveData<NetworkState> =
        MediatorLiveData<NetworkState>().apply {
            this.addSource(Transformations.switchMap(feedRepo) { it.networkState }) {
                this.postValue(it)
            }
            this.addSource(Transformations.switchMap(rssRepo) { it.networkState }) {
                this.postValue(it)
            }
        }

    override fun getFeedResponse(): LiveData<List<RssItemDto>> =
        Transformations.switchMap(feedRepo) {
            it.onSuccess
        }

    override fun getRssResponse(): LiveData<List<KhabarItemDto>> =
        Transformations.switchMap(rssRepo) {
            it.onSuccess
        }

    override fun getFavoriteResponse(): LiveData<List<RssItemDto>> =
        localRepository.getFavoriteRss()

    override fun requestFeed(page : String) {
        feed.postValue(page)
    }

    override fun requestRss(page : String) {
        rss.postValue(page)
    }

    override fun updateRssItem(rssItemDto: RssItemDto) {
        localRepository.updateRss(rssItemDto)
    }

    override fun updateKhabarItem(khabarItemDto: KhabarItemDto) {
        localRepository.updateFeed(khabarItemDto)
    }

    private val feedRepo = Transformations.map(feed) {
        rssRepository.getRss(it)
    }

    private val rssRepo = Transformations.map(rss) {
        khabarOnlineRssRepository.getRss(it)
    }

}
