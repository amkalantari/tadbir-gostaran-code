package com.amiir.testapp.ui.feedTabs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amiir.testapp.R
import com.amiir.testapp.databinding.AdapterFeedItemBinding
import com.core.dto.javaRss.RssItemDto

class FeedAdapter (
    private val itemClickCallback: ((RssItemDto) -> Unit),
    private val likeClickCallback: ((RssItemDto) -> Unit)
) : ListAdapter<RssItemDto, FeedAdapter.ViewHolder>(
    RssItemDto.RssItemDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val dataBinding =
            AdapterFeedItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private var dataBinding: AdapterFeedItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(item: RssItemDto) {
            dataBinding.item = item

            dataBinding.like.setOnClickListener {
                item.isLike = !item.isLike

                if (item.isLike){
                dataBinding.like.setBackgroundColor(dataBinding.root.resources.getColor(R.color.red))
                }else{
                dataBinding.like.setBackgroundColor(dataBinding.root.resources.getColor(R.color.black))
                }

                likeClickCallback(item)
            }

            dataBinding.root.setOnClickListener {
                itemClickCallback(item)
            }
        }

    }

}