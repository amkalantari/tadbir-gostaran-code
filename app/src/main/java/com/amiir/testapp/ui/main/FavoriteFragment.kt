package com.amiir.testapp.ui.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.amiir.testapp.R
import com.amiir.testapp.databinding.FragmentFavoriteBinding
import com.amiir.testapp.ui.feedTabs.adapter.FeedAdapter
import com.amiir.testapp.ui.main.viewModel.MainViewModel
import com.core.base.ParentSharedFragment

class FavoriteFragment : ParentSharedFragment<MainViewModel, FragmentFavoriteBinding>() {

    private val adapter: FeedAdapter by lazy {
        FeedAdapter( {

        },{
            viewModel.updateRssItem(it)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getFavoriteResponse().observe(this, Observer {
            dataBinding.isEmpty = it.isEmpty()
            adapter.submitList(it)
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.requestFeed("1")
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.recycleView.adapter = adapter
    }

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getResourceLayoutId(): Int = R.layout.fragment_favorite

}
