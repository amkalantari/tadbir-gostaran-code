package com.amiir.testapp.ui

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.amiir.testapp.R
import com.tuyenmonkey.mkloader.MKLoader


class WebViewActivity : AppCompatActivity() {

    lateinit var url: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_web_view)

        if (intent.hasExtra("url")) {
            url = intent.getStringExtra("url")!!
        }

        var webView = findViewById<WebView>(R.id.webview)

        var loading = findViewById<MKLoader>(R.id.loading)

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.loadUrl(url)

        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                loading.visibility = View.GONE
            }
        }

    }

}