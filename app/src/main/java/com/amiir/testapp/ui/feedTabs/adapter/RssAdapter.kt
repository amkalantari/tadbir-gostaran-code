package com.amiir.testapp.ui.feedTabs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amiir.testapp.R
import com.amiir.testapp.databinding.AdapterRssItemBinding
import com.bumptech.glide.Glide
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarItemDto

class RssAdapter (
    private val itemClickCallback: ((KhabarItemDto) -> Unit)
) : ListAdapter<KhabarItemDto, RssAdapter.ViewHolder>(
    KhabarItemDto.RssItemDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val dataBinding =
            AdapterRssItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private var dataBinding: AdapterRssItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(item: KhabarItemDto) {
            dataBinding.item = item
            dataBinding.root.setOnClickListener {
                itemClickCallback(item)
            }
            Glide.with(dataBinding.root.context).load(item.enclosure!!.url).into(dataBinding.image)
        }

    }

}