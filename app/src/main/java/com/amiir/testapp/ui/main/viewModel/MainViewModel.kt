package com.amiir.testapp.ui.main.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import com.core.base.BaseViewModel
import com.core.dto.javaRss.RssFeedDto
import com.core.dto.javaRss.RssItemDto
import com.core.dto.khabarOnline.KhabarFeedDto
import com.core.dto.khabarOnline.KhabarItemDto

abstract class MainViewModel (application: Application) : BaseViewModel(application){

    abstract fun getFeedResponse() : LiveData<List<RssItemDto>>

    abstract fun requestFeed(page : String)

    abstract fun getRssResponse() : LiveData<List<KhabarItemDto>>

    abstract fun getFavoriteResponse() : LiveData<List<RssItemDto>>

    abstract fun requestRss(page : String)

    abstract fun updateRssItem(rssItemDto: RssItemDto)

    abstract fun updateKhabarItem(khabarItemDto: KhabarItemDto)

}