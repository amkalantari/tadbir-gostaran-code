package com.amiir.testapp.ui.main

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import com.amiir.testapp.R
import com.amiir.testapp.databinding.FragmentFeedBinding
import com.amiir.testapp.ui.main.TabsPagerAdapter
import com.amiir.testapp.ui.main.viewModel.MainViewModel
import com.core.base.ParentSharedFragment
import kotlinx.android.synthetic.main.fragment_feed.*

class FeedFragment : ParentSharedFragment<MainViewModel, FragmentFeedBinding>() {

    private lateinit var tabsPagerAdapter: TabsPagerAdapter

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getResourceLayoutId(): Int = R.layout.fragment_feed

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val elevation = resources.getDimensionPixelSize(R.dimen.tabs_elevation).toFloat()
        if (tabs != null)
            ViewCompat.setElevation(tabs, elevation)

        tabsPagerAdapter = TabsPagerAdapter(
            context!!,
            childFragmentManager!!
        )

        view_pager.adapter = tabsPagerAdapter
        tabs.setupWithViewPager(view_pager)

        view_pager.currentItem = 0

    }


}