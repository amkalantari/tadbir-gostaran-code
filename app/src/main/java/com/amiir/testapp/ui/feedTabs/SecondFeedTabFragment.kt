package com.amiir.testapp.ui.feedTabs

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.amiir.testapp.R
import com.amiir.testapp.databinding.FragmentSecondFeedTabBinding
import com.amiir.testapp.ui.WebViewActivity
import com.amiir.testapp.ui.feedTabs.adapter.RssAdapter
import com.amiir.testapp.ui.main.viewModel.MainViewModel
import com.core.base.ParentSharedFragment

class SecondFeedTabFragment  : ParentSharedFragment<MainViewModel, FragmentSecondFeedTabBinding>() {

    private val adapter: RssAdapter by lazy {
        RssAdapter {
            startActivity(Intent(requireActivity(),WebViewActivity::class.java).putExtra("url",it.link))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getRssResponse().observe(this, Observer {
            it?.let {
                dataBinding.isEmpty = it.isEmpty()
                adapter.submitList(it)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.recycleView.adapter = adapter
    }

    override fun getResourceLayoutId(): Int = R.layout.fragment_second_feed_tab

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.requestRss("1")
    }

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

}