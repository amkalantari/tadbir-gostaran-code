package com.amiir.testapp.ui.main

import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.amiir.testapp.R
import com.amiir.testapp.databinding.ActivityMainBinding
import com.amiir.testapp.di.DaggerAppComponent
import com.amiir.testapp.di.DatabaseModule
import com.amiir.testapp.di.NetworkModule
import com.amiir.testapp.ui.main.viewModel.MainViewModel
import com.amiir.testapp.ui.main.viewModel.MainViewModelImpl
import com.core.base.ParentActivity
import com.core.repository.KhabarOnlineRssRepository
import com.core.repository.LocalRepository
import com.core.repository.RssRepository
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : ParentActivity<MainViewModel,ActivityMainBinding>() {

    @Inject
    lateinit var rssRepository: RssRepository

    @Inject
    lateinit var khabarOnlineRssRepository: KhabarOnlineRssRepository

    @Inject
    lateinit var localRepository: LocalRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.navHost) as NavHostFragment? ?: return

        val navController = host.navController

        navbar?.setupWithNavController(navController)

        val navBarElevation =
            resources.getDimensionPixelSize(R.dimen.main_navbar_elevation).toFloat()
        if (navbar != null)
            ViewCompat.setElevation(navbar, navBarElevation)

    }

    override fun getResourceLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModelClass(): Class<MainViewModel> =  MainViewModel::class.java


    override fun getFactory(): ViewModelProvider.Factory {
        return object : ViewModelProvider.NewInstanceFactory() {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return MainViewModelImpl(
                    application!!,
                    rssRepository,
                    khabarOnlineRssRepository,
                    localRepository = localRepository
                ) as T
            }
        }
    }

    override fun inject() {
        DaggerAppComponent.builder()
            .networkModule(NetworkModule(application))
            .databaseModule(DatabaseModule())
            .build()
            .inject(this)
    }
}
