package com.amiir.testapp.di

import com.amiir.testapp.App
import com.amiir.testapp.ui.main.FeedFragment
import com.amiir.testapp.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        DatabaseModule::class]
)
interface AppComponent {

    fun inject(app: App)
    fun inject(app: MainActivity)
    fun inject(app: FeedFragment)

//    @Component.Builder
//    interface Builder {
//        @BindsInstance
//        fun application(App: Application): Builder
//        fun build(): AppComponent
//    }

}
