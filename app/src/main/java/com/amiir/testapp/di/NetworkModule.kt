package com.amiir.testapp.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.core.api.KhabarOnlineRssApi
import com.core.api.RssApi
import com.core.dao.KhabarDao
import com.core.dao.RssDao
import com.core.repository.*
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


@Module
class NetworkModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    fun provideOkHttp(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(3, TimeUnit.MINUTES)
            .readTimeout(3, TimeUnit.MINUTES)
            .writeTimeout(3, TimeUnit.MINUTES)
        return httpClient.build()
    }

    @Singleton
    @Provides
    fun provideConnectivityManager(app: Application) = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Provides
    @Singleton
    @Named("retrofit_java")
    fun provideRetrofitJava(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://howtodoinjava.com/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    @Named("retrofit_khabar_online")
    fun provideRetrofitOnline(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://www.khabaronline.ir/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .client(client)
            .build()
    }

    @Singleton
    @Provides
    fun provideRssApi(@Named("retrofit_java") retrofit: Retrofit): RssApi {
        return retrofit.create(RssApi::class.java)
    }

    @Singleton
    @Provides
    fun provideRssApiKhabarOnline(@Named("retrofit_khabar_online") retrofit: Retrofit): KhabarOnlineRssApi {
        return retrofit.create(KhabarOnlineRssApi::class.java)
    }

    @Singleton
    @Provides
    fun provideRssRepository(rssApi: RssApi,localRepository: LocalRepository): RssRepository {
        return RssRepositoryImpl(rssApi,localRepository)
    }

    @Singleton
    @Provides
    fun provideLocalRepository(feedDao: KhabarDao, rssDao: RssDao): LocalRepository {
        return LocalRepositoryImpl(feedDao,rssDao)
    }


    @Singleton
    @Provides
    fun provideKhabarOnlineRssRepository(rssApi: KhabarOnlineRssApi,localRepository: LocalRepository): KhabarOnlineRssRepository {
        return KhabarOnlineRssRepositoryImpl(rssApi,localRepository)
    }

}

