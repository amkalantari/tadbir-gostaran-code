package com.amiir.testapp.di

import android.app.Application
import com.core.db.TestDb
import androidx.room.Room
import com.core.dao.KhabarDao
import com.core.dao.RssDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): TestDb {
        return Room
            .databaseBuilder(app, TestDb::class.java, "cache.db")
            .fallbackToDestructiveMigration()
            .build()
    }


    @Singleton
    @Provides
    fun provideRssDao(db: TestDb): RssDao {
        return db.rssDao()
    }

    @Singleton
    @Provides
    fun provideFeedDao(db: TestDb): KhabarDao {
        return db.khabarDao()
    }
}
