package com.amiir.testapp

import android.app.Application

class App : Application() {

    private fun handleUncaughtExceptionHandler(thread: Thread?, e: Throwable?) {
        try {
            if (e != null) {
                var exceprionMessage = "****************************************************\n"
                exceprionMessage += "exception ClassName    : " + e.javaClass.simpleName + "\n"
                if (e.stackTrace.isNotEmpty()) {
                    exceprionMessage += "exception MethodName   : " + e.stackTrace[0].methodName + "\n"
                    exceprionMessage += "exception LineNumber   : " + e.stackTrace[0].lineNumber + "\n"
                }
                exceprionMessage += "   is Thread Alive " + thread?.isAlive
                exceprionMessage += "   is Thread State " + thread?.state
                exceprionMessage += "exception Message      : " + e.message + "\n"
                exceprionMessage += "****************************************************\n"
            }
        } catch (ex: Exception) {
            //send to firebase
        }
    }

    override fun onCreate() {
        super.onCreate()
        Thread.UncaughtExceptionHandler(this::handleUncaughtExceptionHandler)
    }
}